import React from 'react'

export default class ContributorList extends React.Component {
  render () {
    return <div className='contributor text-center'>
      <img className='avatar' src={this.props.photo} alt={this.props.photo} />
      <h4> <br/>
        {this.props.firstName} <br/>
        {this.props.lastName}
      </h4>
    </div>
  }
}
