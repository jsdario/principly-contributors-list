import React from 'react'

import Contributor from './Contributor.jsx'

export default class ContributorList extends React.Component {
  render () {
    let contributors = this.props.contributors
    let inviteContributors = (
      <a href='javascript:void(0)'><h3>Invite a contributor <i className='fa fa-share'></i></h3></a>
    )

    let allContributors = (
      <span><a href='javascript:void(0)'>See all contributors...</a>
      <br/></span>
    )

    return <div className='contributors-list text-center'>
      <h1>Contributors</h1>
      {(contributors.length < 1) ? inviteContributors : null}
      {contributors.slice(0, 6).map(function (contributor) {
        return <Contributor firstName={contributor.firstName}
          lastName={contributor.lastName}
          photo={contributor.photo} />
      })}
      <br/>
      {(contributors.length > 6) ? allContributors : null}
    </div>
  }
}

ContributorList.propTypes = {
  requiredArrayOf: React.PropTypes.arrayOf(React.PropTypes.object)
}
