
import $ from 'jquery'
import React from 'react'
import ReactDOM from 'react-dom'

import ContributorList from './contributors-list.jsx'

import './styles/index.scss'

$.get('/api/contributors').done(function (result) {
  ReactDOM.render(
    <ContributorList contributors={result}/>,
    document.getElementById('app')
  )
})
