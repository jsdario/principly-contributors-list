#!/usr/bin/env node

'use strict'

let faker = require('faker') // generate random contributors
let express = require('express')

let app = express()

let httpProxy = require('http-proxy')

let webpackProxy = httpProxy.createProxyServer()

let isProduction = process.env.NODE_ENV === 'production'
let port = isProduction ? process.env.PORT : 3000

if (isProduction) {
  app.use(express.static('./build'))
} else {
  app.use(express.static('./build.dev'))
}

app.use('fonts', express.static('./public/fonts'))

app.all('/build.dev/*', function (req, res, next) {
  // hot reloading express + webpack proxy
  webpackProxy.web(req, res, {
    target: 'http://localhost:8080'
  })
})

app.use('/api/contributors', function (req, res, next) {
  let contributors = []

  // random number of contributors
  let N = Math.ceil(Math.random() * 12)

  if (Math.random() < 0.2) {
    // 20% of funds do not have yet Contributors
    return res.json(contributors)
  }

  for (let n = 0; n < N; n++) {
    contributors.push({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      photo: faker.image.avatar()
    })
  }

  return res.json(contributors)
})

app.use('/api/bonus', function (req, res, next) {
  var fund = {
    contributions: [
      { amount: 100, contributor: { id: 2, firstName: 'Carl', lastName: 'Spencer', photo: '02356.jpg' } },
      { amount: 700, contributor: { id: 9, firstName: 'Jack', lastName: 'Bauer', photo: '2298356.jpg' } },
      { amount: 200, contributor: { id: 2, firstName: 'Carl', lastName: 'Spencer', photo: '02356.jpg' } }
    ]
  }

  var contributors = []

  fund.contributions.forEach(function (contribution) {
    contributors.push(contribution.contributor)
  })

  return res.json(contributors)
})

app.listen(port, function () {
  console.log('Server running on port ' + port)
})
