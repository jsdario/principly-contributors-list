
> ### Clone, install and try
1. `git clone https://bitbucket.org/jsdario/principly-contributors-list`
2. `npm install # will install local dependencies`
3. `npm run pro # will launch app on port 3000`
Also `npm run build` would rebuild the production version and `npm run dev` would
start webpack hot-reloading development server.

![result caption](./result.png)
_Result caption with photos from AWS utilities and fake data._

> ### Solution Comments
- I used "faker" npm module to generate random amounts of contextual data for testing
different contributors lists. Each refresh or server request will load a random set of them, so we can see the cases where there are no contributors, less than six and more than six.
- These fake contributors are retrieved from a simple but realistic express api. Take a look on `server.js`. http://localhost:3000/api/contributors and refresh a couple of times to see what happens.
- Bootstrap is used through `@extend` SASS directive or explicit classNames. However, given the simplicity of the problem, little styling has been applying and some CSS rules are enough to keep it responsive.
- _View all contributors_ is a tip for users to know there are more contributors than those shown. However it does not link anywhere by the moment.
- Same happens with _invite a contributor_ link
- I used ES6 on front-end with babel transpiler.
- Webpack is used for production and development with _hot realoading_ modules. Other building system may be used to compile from ES6 or SASS.

> #### Bonus Challenge
Its solution is coded in server.js and you can watch it run on http://localhost:3000/api/bonus

_Jesus Dario_


# Programming Test ­ ContributorsList

## Scenario
We are building a web­app to manage college savings funds. Each fund​ can receive multiple
contributions ​from different contributors.​You task is to display the list of contributors for a given fund.

## Instructions
Write a Contributors List​component that displays a list of the contributors.

Assume the component will receive the list of contributors as a single “prop” called “contributors”,
following this structure:

https://gist.github.com/anonymous/c18e248935264af8b052

## Requirements

- [x] use React and Bootstrap (+ babel es6, sass, express and webpack)
- [x] the list should look good even on mobile
- [x] if there are no contributors display a link “Invite a contributor”
- [x] only display the first 6 contributors. if there are more than 6, ignore the rest
- [x] have fun! :)

## Bonus challenge :done:

If you have time and you feel like it, see if you can do this extra challenge: extract the list of unique contributors ​from the following data structure:

https://gist.github.com/anonymous/040bb43aa95c198dd930
